// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use rocket::serde::{Deserialize, Serialize};

#[derive(Responder, Debug, Deserialize, Serialize)]
pub struct MessageResponse {
    /// This is a message from the server.
    pub message: String,
}
