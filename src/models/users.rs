// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use rocket::request;
use rocket::serde::{Deserialize, Serialize};
use rocket::http::{CookieJar, Status};
use diesel::prelude::*;
use std::cmp::{Ord, Eq, PartialOrd, PartialEq};
use crate::db::Database;

/// User information to be retrieved from the Gitlab API.
#[derive(Deserialize, Debug)]
pub struct UserInfo {
    pub name: String,
    pub nickname: String,
    pub email: String,
    pub picture: String,
    pub groups: Vec<String>,
}

#[derive(Queryable, Selectable, Serialize, Ord, Eq, PartialEq, PartialOrd, Debug)]
#[diesel(table_name = crate::schema::users)]
#[serde(crate = "rocket::serde")]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub fullname: String,
    pub developer: bool,
    pub picture: String,
}

#[rocket::async_trait]
impl<'r> request::FromRequest<'r> for User {
    type Error = ();

    async fn from_request(request: &'r request::Request<'_>) -> request::Outcome<User, ()> {
        let cookies = request
            .guard::<&CookieJar<'_>>()
            .await
            .expect("request cookies");
        if let Some(cookie) = cookies.get_private("user") {
            let db: Database = request.guard::<Database>().await.expect("Database to be working");

            return match crate::db::users::find(db, cookie.value().parse().unwrap()).await {
                Ok(user) => request::Outcome::Success(user),
                Err(_) => request::Outcome::Forward(Status::Unauthorized)
            }
        }

        request::Outcome::Forward(Status::Unauthorized)
    }
}
