// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use rocket::serde::Serialize;

#[derive(Serialize)]
#[serde(crate = "rocket::serde", remote = "poreader::State")]
pub enum StateDef {
    Empty,
    NeedsWork,
    Final,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub enum Message {
    Simple {
        origin: String,
        translation: Option<String>,
    },
    Plural {
        singular: String,
        plural: String,
        values: Vec<String>,
    }
}

impl From<&poreader::Message> for Message {
    fn from(message: &poreader::Message) -> Self {
        match message {
            poreader::Message::Simple { id, text } => {
                Message::Simple {
                    origin: id.to_string(),
                    translation: text.clone(),
                }
            },
            poreader::Message::Plural(plural) => {
                Message::Plural {
                    singular: plural.singular().to_string(),
                    plural: plural.plural().to_string(),
                    values: plural.values().clone(),
                }
            }
        }
    }
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Unit {
    pub message: Message,
    pub context: Option<String>,
    pub locations: Vec<String>,
    pub comments: Vec<String>,
    #[serde(with = "StateDef")]
    pub state: poreader::State,
}
